import datetime
import logging
import re
from typing import Dict, List, Optional, Union
from urllib.parse import quote as urlquote
from urllib.parse import urlencode

import requests
from bs4 import BeautifulSoup

CONNECT_DOMAIN = "https://connect.garmin.com"
CONNECT_URL = f"{CONNECT_DOMAIN}/modern/"
SSO_DOMAIN = "https://sso.garmin.com"
SSO_URL = f"{SSO_DOMAIN}/sso/signin"
SSO_PARAMS = {
    "service": CONNECT_URL,
    "webhost": CONNECT_URL,
    "source": f"{CONNECT_DOMAIN}/signin/",
    "redirectAfterAccountLoginUrl": CONNECT_URL,
    "redirectAfterAccountCreationUrl": CONNECT_URL,
    "gauthHost": "https://sso.garmin.com/sso",
    "locale": "en_US",
    "id": "gauth-widget",
    "cssUrl": f"{CONNECT_DOMAIN}/gauth-custom-v1.2-min.css",
    "privacyStatementUrl": "https://www.garmin.com/en-US/privacy/connect/",
    "clientId": "GarminConnect",
    "rememberMeShown": "true",
    "rememberMeChecked": "false",
    "createAccountShown": "true",
    "openCreateAccount": "false",
    "displayNameShown": "false",
    "consumeServiceTicket": "false",
    "initialFocus": "true",
    "embedWidget": "false",
    "generateExtraServiceTicket": "true",
    "generateTwoExtraServiceTickets": "true",
    "generateNoServiceTicket": "false",
    "globalOptInShown": "true",
    "globalOptInChecked": "false",
    "mobile": "false",
    "connectLegalTerms": "true",
    "showTermsOfUse": "false",
    "showPrivacyPolicy": "false",
    "showConnectLegalAge": "false",
    "locationPromptShown": "true",
    "showPassword": "true",
    "useCustomHeader": "false",
    "mfaRequired": "false",
    "rememberMyBrowserShown": "false",
    "rememberMyBrowserChecked": "false",
}
SSO_REFERER = SSO_URL + "?" + urlencode({k: urlquote(v) for k, v in SSO_PARAMS.items()})

ST_TOKEN_RE = re.compile(r"ST-[a-zA-Z0-9-]+")

logger = logging.getLogger(__name__)


DATE_TYPE = Union[datetime.date, datetime.datetime, str]


def date_type_to_str(d: DATE_TYPE) -> str:
    if isinstance(d, datetime.datetime):
        return d.date().isoformat()
    if isinstance(d, datetime.date):
        return d.isoformat()
    return d


class Garymin:
    URL_DAILY_MOVEMENT = "proxy/wellness-service/wellness/dailyMovement/{account}"
    URL_DAILY_STEPS = "proxy/wellness-service/wellness/dailySummaryChart/{account}"
    URL_DAILY_STRESS = "proxy/wellness-service/wellness/dailyStress/{date}"
    URL_DEVICE_INFO = "proxy/device-service/deviceservice/device-info/active/{account}"
    URL_HEART_RATE = "proxy/wellness-service/wellness/dailyHeartRate/{account}"
    URL_SLEEP_DATA = "proxy/wellness-service/wellness/dailySleepData/{account}"

    URL_REQUEST_OLD_DATA = "proxy/wellness-service/wellness/epoch/request/{date}"

    def __init__(self, account: str):
        self.account = account
        self._authenticated = False
        self.session = requests.Session()

    def login(self, email: str, password: str):
        response = self.session.get(SSO_URL, params=SSO_PARAMS, allow_redirects=False)
        response.raise_for_status()

        soup = BeautifulSoup(response.text, features="html.parser")
        element = soup.find("input", attrs={"name": "_csrf"})
        if element is None:
            raise ValueError("No CSRF element found", response.text)

        csrf_token = element.attrs["value"]

        post_data = {
            "username": email,
            "password": password,
            "embed": "false",
            "_csrf": csrf_token,
        }
        headers = {"Origin": SSO_DOMAIN, "Referer": SSO_REFERER}
        response = self.session.post(
            SSO_URL,
            params=SSO_PARAMS,
            data=post_data,
            allow_redirects=False,
            headers=headers,
        )
        response.raise_for_status()

        service_ticket_match = ST_TOKEN_RE.search(response.text)
        if service_ticket_match is None:
            raise ValueError(
                f"No service token found. {ST_TOKEN_RE.pattern!r}", response.text
            )

        service_ticket = service_ticket_match[0]

        headers = {"Referer": SSO_REFERER}
        response = self.session.get(
            CONNECT_URL,
            params={"ticket": service_ticket},
            headers=headers,
            allow_redirects=False,
        )
        response.raise_for_status()

        if not response.is_redirect:
            raise ValueError("Expected redirect to Garmin Connect page.")

        response = self.session.get(
            CONNECT_URL,
            headers=headers,
        )
        response.raise_for_status()

        self._authenticated = True

    def _get_data(self, url: str, params: Optional[Dict] = None, method="GET") -> Dict:
        if not self.authenticated:
            raise ValueError("Not authenticated. Call 'Garymin.login()' first.")

        headers = {
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "Referer": CONNECT_URL,
            "NK": "NT",
            "TE": "Trailers",
            "X-Requested-With": "XMLHttpRequest",
        }
        url = CONNECT_URL + url.format(account=self.account)
        caller = self.session.post if method == "POST" else self.session.get
        response = caller(url, params=params, headers=headers, allow_redirects=False)
        response.raise_for_status()

        return response.json()

    def get_daily_movement(self, date: DATE_TYPE) -> Dict:
        return self._get_data(
            self.URL_DAILY_MOVEMENT, params={"calendarDate": date_type_to_str(date)}
        )

    def get_daily_steps(self, date: DATE_TYPE) -> List[Dict]:
        return self._get_data(
            self.URL_DAILY_STEPS, params={"date": date_type_to_str(date)}
        )

    def get_daily_stress(self, date: DATE_TYPE) -> Dict:
        return self._get_data(self.URL_DAILY_STRESS.format(date=date_type_to_str(date)))

    def get_device_info(self) -> Dict:
        return self._get_data(self.URL_DEVICE_INFO)

    def get_heart_rate(self, date: DATE_TYPE) -> Dict:
        return self._get_data(
            self.URL_HEART_RATE, params={"date": date_type_to_str(date)}
        )

    def get_sleep_data(self, date: DATE_TYPE) -> Dict:
        return self._get_data(
            self.URL_SLEEP_DATA, params={"date": date_type_to_str(date)}
        )

    def request_old_data(self, date: DATE_TYPE) -> Dict:
        return self._get_data(
            self.URL_REQUEST_OLD_DATA.format(date=date_type_to_str(date)), method="POST"
        )

    @property
    def authenticated(self):
        return self._authenticated
