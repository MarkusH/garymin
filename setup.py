# -*- coding: utf-8 -*-

import os
from pathlib import Path

from pkg_resources.extern.packaging.version import Version
from setuptools import find_packages, setup

# a version must be PEP 440 compliant
__version__ = Version("0.0.1")


def cwd() -> Path:
    return Path(os.path.dirname(__file__))


def read(path: str) -> str:
    filepath: Path = cwd() / path
    with open(filepath.absolute(), "r", encoding="utf-8") as f:
        return f.read()


setup(
    name="garymin",
    author="Markus Holtermann",
    author_email="info+garymin@markusholtermann.eu",
    description="Python interface over the Garmin API",
    long_description=read("README.md"),
    version=str(__version__),
    packages=find_packages(exclude=["tests.*"]),
    install_requires=[
        "beautifulsoup4>=4.8,<5",
        "requests==2.22.0",
    ],
    extras_require={
        "testing": [
            "pytest>=5,<6",
            "pytest-black==0.3.7",
            "pytest-flake8==1.0.4",
            "pytest-isort==0.3.1",
            "pytest-mypy==0.4.2",
        ],
    },
    python_requires=">=3.7",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
    ],
)
