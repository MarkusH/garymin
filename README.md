# Garymin

Python API for Garmin Connect

## Example

```python
import datetime
import os
import pprint

from garymin import Garymin


def main():
    account = os.environ["GARMIN_ACCOUNT"]
    email = os.environ["GARMIN_EMAIL"]
    password = os.environ["GARMIN_PASSWORD"]

    garymin = Garymin(account)
    garymin.login(email, password)
    today = datetime.date.today()
    pprint.pprint(garymin.get_device_info())
    pprint.pprint(garymin.get_heart_rate(today - datetime.timedelta(days=1)))
    pprint.pprint(garymin.get_sleep_data(today))


if __name__ == "__main__":
    main()
```
